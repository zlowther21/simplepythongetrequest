__author__ = 'zlowther21'

import fnmatch
import os
import sys


def main():
    arguments_count = len(sys.argv)
    if arguments_count == 2 and sys.argv[1] == "--help":
        displayHelp()
    elif arguments_count < 3:
        print """
Error: Function Misused - Consult Help Page (python search.py --help)

Explanation: The 'search' command requires at least 2 parameters to work correctly. Please review the Help section by
typing 'python search.py --help' to learn the correct usage of the 'search' command.

"""
    else:
        drive = sys.argv[1]
        extension = sys.argv[2]
        if len(sys.argv) != 4:
            search(drive, extension)
        else:
            search(drive, extension, sys.argv[3])


def search(drive, extension, string=None):
    matches = []
    for root, dirnames, filenames in os.walk(drive):
        for filename in fnmatch.filter(filenames, '*.' + extension):
            matches.append(os.path.join(root, filename))
    if len(matches) is 0:
        print "No Files Found in the folder with the the given extension"
        return

    if string is not None:
        seekString(matches, string)
    else:
        for match in matches:
            print match

def seekString(matches, string):
    counter = 0
    for match in matches:
        if string in open(match).read():
            print match
            counter += 1
    if counter is 0:
        print "No Files in the folder with the given extension contain the string: " + string
def displayHelp():
    print """
Hi and Welcome to the Search Command Help Page.

The 'search' command requires 2 parameters:
    * Parameter 1 -> The Drive or Directory In which you wish to search for the file, for example '/home'
    * Parameter 2 -> The Extension of the file you are searching for, for example 'docx'
    * An optional third parameter allows you to search for a given string found in files returned by
      searching the given drive and the given file extension. The program will iterate over the files
      found and return only files that contain the given string.

    ***Example Usage***
    Normal Usage: python search.py /home/myuser txt
    With Third Parameter to search for a string: python search.py /home/myuser txt samplestring


    If you have further questions regarding the usage of python, please visit https://www.python.org/doc/
     """


if __name__ == "__main__":
    main()
