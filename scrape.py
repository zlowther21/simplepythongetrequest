import netifaces
import urllib2

def main():
    interfaces = netifaces.interfaces()
    collection = {}
    print "Network Interfaces Information: "
    print "------------------------------------"
    for i in interfaces:
        collection[i] = netifaces.ifaddresses(i)

    for i in collection:
        print "Network Interface: " + i
        if netifaces.AF_LINK in collection[i]:
            if len(collection[i][netifaces.AF_LINK]) == 1:
                print "\tMAC Address: " + collection[i][netifaces.AF_LINK][0]['addr']
        if netifaces.AF_INET in collection[i]:
            if len(collection[i][netifaces.AF_INET]) > 0:
                if 'addr' in collection[i][netifaces.AF_INET][0]:
                    print "\tIP Address: " + collection[i][netifaces.AF_INET][0]['addr']
                if 'netmask' in collection[i][netifaces.AF_INET][0]:
                    print "\tSubnet Mask: " + collection[i][netifaces.AF_INET][0]['netmask']
                if 'broadcast' in collection[i][netifaces.AF_INET][0]:
                    print "\tBroadcast Address: " + collection[i][netifaces.AF_INET][0]['broadcast']
                gateway = netifaces.gateways()
                if 'default' in gateway and gateway['default'][netifaces.AF_INET][1] == i> 0:
                    print "\tGateway: " + gateway['default'][netifaces.AF_INET][0]

        print "\n"

    print "DNS Servers"
    print "-------------------------"
    with open('/etc/resolv.conf', 'r') as file:
        data = file.readlines()
        for line in data:
            if line.startswith("nameserver"):
                print line

    print "\n"
    my_ip = urllib2.urlopen('http://icanhazip.com').read()
    print "Public IP Address"
    print "-------------------------"
    print my_ip


if __name__ == "__main__":
    main()
