__author__ = 'Zachary Lowther'

import sys
import urllib2
import webbrowser



def main():
    arguments_count = len(sys.argv)
    if arguments_count == 2 and sys.argv[1] == "--help":
        displayHelp()
    elif arguments_count < 3:
        print """
Error: Function Misused - Consult Help Page (python get.py --help)

Explanation: The 'get' command requires at least 2 parameters to work correctly. Please review the Help section by
typing 'python get.py --help' to learn the correct usage of the 'get' command.

"""
    else:

        try:
            document = sys.argv[1]
            web_address = sys.argv[2]
            if str(web_address).endswith("/"):
                web_address = str(web_address)[:-1]
            if 'http://' not in str(web_address) or 'https://' not in str(web_address):
                web_address = "http://" + str(web_address)
            response = urllib2.urlopen(web_address + "/" + document)
            print response.read()

            if len(sys.argv) == 4 and sys.argv[3] == "--browser":
                webbrowser.open(web_address + "/" + document, new=2)
            elif len(sys.argv) == 4 and not sys.argv[3] == "--browser":
                print "Error: The third parameter is invalid. Try --browser instead."
        except urllib2.URLError:
            print "Either the page you requested doesn't exist or the web address you entered was invalid"
        except urllib2.HTTPError:
            print "Http Error"

def displayHelp():
    print """
Hi and Welcome to the Get Command Help Page.

The 'get' command requires 2 parameters:
    * Parameter 1 -> HTML Page you wish to load from a given web server, for example 'index.html'
    * Parameter 2 -> Web Address of the server to which you wish to make a request, for example 'http://www.google.com'
        -- Special Note -> You do not need to worry about adding 'http' or 'www' to the web address if you do not want to do so
    * An optional third parameter allows you to open a web browser and show the result of your search. Use the '--browser' flag.

    ***Example Usage***
    Normal Usage: python get.py index.html http://google.com
    With Third Parameter to Open Web Browser: python get.py index.html byu.edu --browser


    If you have further questions regarding the usage of python, please visit https://www.python.org/doc/
     """

if __name__ == "__main__":
    main()




